# In another shell
export JAVA_HOME=/usr/lib/jvm/java-11-openjdk-amd64
export AXIS2_HOME=$PWD/axis2-1.7.9

# Change argument names
sed -i 's/args0/min/' axis2-1.7.9/repository/services/RandomService/META-INF/RandomService.wsdl
sed -i 's/args1/max/' axis2-1.7.9/repository/services/RandomService/META-INF/RandomService.wsdl

$AXIS2_HOME/bin/wsdl2java.sh -uri $AXIS2_HOME/repository/services/RandomService/META-INF/RandomService.wsdl -p if3040.soap -d adb -s -o client
cp src/if3040/soap/RandomServiceClient.java client/src/if3040/soap/
cd client/src/
javac -cp $AXIS2_HOME/lib/\* if3040/soap/RandomServiceClient.java if3040/soap/RandomServiceStub.java

java -cp .:$AXIS2_HOME/lib/\* if3040.soap.RandomServiceClient
java -cp .:$AXIS2_HOME/lib/\* if3040.soap.RandomServiceClient -1 1
java -cp .:$AXIS2_HOME/lib/\* if3040.soap.RandomServiceClient
