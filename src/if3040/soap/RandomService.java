package if3040.soap;

public class RandomService {
    private int min = 1;
    private int max = 100;
        
    public void setBounds( int min, int max ) {
	      System.out.println("setBounds called");
        if( min <=max ) {
            this.min = min;
            this.max = max;
        }
    }
    
    public int nextRandom() {
	      System.out.println("nextRandom called");
        double alea = Math.random();
        return min + ( int ) (( max - min + 1 ) * alea );
    }
}
